# import the pygame module, so you can use it
import sys, pygame
from pygame.locals import *
 
# define a main function
def main():
     
    # initialize the pygame module
    pygame.init()
    # load and set the logo
    logo = pygame.image.load("rb.png")
    logo2 = pygame.transform.scale(logo,(32,32))
    logo = pygame.transform.scale(logo,(32,32))
    pygame.display.set_icon(logo2)
    pygame.display.set_caption("nice game")
    
    clock = pygame.time.Clock()

    # create a surface on screen that has the size of 240 x 180
    screen = pygame.display.set_mode((800,600))
    
     
    # define a variable to control the main loop
    running = True
     
    # main loop
    while running:

        block_pos_x, block_pos_y = pygame.mouse.get_pos()

        # event handling, gets all event from the eventqueue
        for event in pygame.event.get():
            # only do something if the event is of type QUIT
            if event.type == pygame.QUIT:
                # change the value to False, to exit the main loop
                sys.exit()
            # add escape key to exit (remove this to make it harder to exit the program)
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                sys.exit() 

        screen.fill((0,0,0))
        screen.blit(logo2,(block_pos_x, block_pos_y))
        pygame.display.update()
        clock.tick(60)

        
    else:
        sys.exit()
     
     
# run the main function only if this module is executed as the main script
# (if you import this as a module then nothing is executed)
if __name__=="__main__":
    # call the main function
    main()